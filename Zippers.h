#include <stdio.h>
#include <algorithm>
#include <vector>
#include <utility>
#include <type_traits>
#include <iostream>
#include <list>
#include <deque>

/*
 * Zips 2 containers into a single container of std::pair<Container1Type, container2Type>.
 * Usually you want the return container type to be a vector, but any container that supports push_back will work, such as std::list, or even QT types.
 * Lists must be of equal size.
 * Usage : "const auto zippedList = zip<std::vector>(intList.cbegin(), intList.cend(), doubleList.begin())"
 */
 
template<template<typename...> class Container, //Return container type, std::vector or std::list or whatever.
         typename Input1It, //First list .begin() and .end()
         typename Input2It, //Second list .begin()
         typename T1 = typename std::iterator_traits<Input1It>::value_type, //Type of object stored in first container
         typename T2 = typename std::iterator_traits<Input2It>::value_type, //Type of object stored in second container
         typename Pair =  std::pair<T1, T2> > //Pair alias, pair of types from each list.
Container<Pair> zip(Input1It container1Begin, Input1It container1End, Input2It container2Begin)
{
    Container<Pair> zippedContainer;
    while (container1Begin != container1End)
    {
        zippedContainer.emplace_back(*container1Begin, *container2Begin);
        
        std::advance(container1Begin, 1);
        std::advance(container2Begin, 1);
    }
    return zippedContainer;
}

/*
 * Unzips a single container of pair structures, (like std::vector<std::pair<T1,T2>> into two containers, again of arbitary type)
 * For example, can go std::vector<std::pair<int, double>> -> std::pair<std::list<int>, std::deque<double>>, which you can then seperate with std::tie or whatever.
 * Usage : "const auto unzipped = unzip<std::list, std::deque>(zippedVec);"
 */

template<template<typename...> class OutputContainerType1, //Type of container we're unzipping first pair element to, anything that supports emplace_back
         template<typename...> class OutputContainerType2, //Type of container we're unzipping second pair element to, anything that supports emplace_back
         typename InputPairContainer, //Container of std::pairs, deduced from input
         typename PairType1 = typename std::tuple_element<0, typename InputPairContainer::value_type>::type, //Element type of the .first of the pairs in container
         typename PairType2 = typename std::tuple_element<1, typename InputPairContainer::value_type>::type, //Element type of the .second of the pairs in container
         typename OutputUnpairedContainer1 = OutputContainerType1<PairType1>, //Combine OutputContainerType with PairType to get OutputContainerType<PairType>, output 1
         typename OutputUnpairedContainer2 = OutputContainerType2<PairType2>> //Combine OutputContainerType with PairType to get OutputContainerType<PairType>, output 2
std::pair<OutputUnpairedContainer1, OutputUnpairedContainer2> unzip(const InputPairContainer& input)
{
    OutputUnpairedContainer1 output1 = {};
    OutputUnpairedContainer2 output2 = {};
    for(const auto& pair : input)
    {
        output1.emplace_back(pair.first);
        output2.emplace_back(pair.second);
    }
    
    return std::make_pair(output1, output2);
}





//////Testing////////
struct Demostruct
{
    double x;
    double y;
    double z;
};

int main()
{
    std::vector<double> inputVector1 = {1.0, 2.0, 3.0, 4.0, 5.0};
    std::list<Demostruct> inputVector2 = {{0.0, 3.0, 1.0}, {1.0, 1.0, 1.0}, {0.0, 0.0, 0.0}, {5.0, 3.0, 1.0}, {0.0, 0.0, 0.0}};
    
    std::cout << "--Zipped--" << std::endl;
    //Zip the 2 vectors into one container.
    const auto zippedVec = zip<std::vector>(inputVector1.cbegin(), inputVector1.cend(), inputVector2.cbegin());
    for(const auto& pair : zippedVec)
    {
        std::cout << "First : " << pair.first << std::endl;
        std::cout << "Second : " << " X:" << pair.second.x << " Y:" << pair.second.y << " Z:" << pair.second.z << std::endl;
    }
    
    std::cout << std::endl;
    std::cout << "--Unzipped--" << std::endl;
    
    //Unzip the vectors, into a list and a deque, just for fun.
    const auto unzipped = unzip<std::list, std::deque>(zippedVec);
    for(const auto& val : unzipped.first)
    {
        std::cout << "First List Val :" << val << std::endl;
    }
    for(const auto& val : unzipped.second)
    {
         std::cout << "Second List Val : " << " X:" << val.x << " Y:" << val.y << " Z:" << val.z << std::endl;
    }
    return 0;
}
